package physicaltestmanager.model.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import com.mycompany.physicaltestmanager.model.PhysicalTest;

class PhysicalTestTest {

	@Test
    public void constructorGetterTestOne() {
        LocalDate dateOfExecution = LocalDate.of(2020, 5, 5);
        PhysicalTest test = new PhysicalTest(1, 50034531, dateOfExecution, 0, 50, 0, 60, 0, 1230);
        assertEquals(Integer.valueOf(1), test.getId(), "Hiba! Nem egyezik a sorszám értéke!");
        assertEquals(50034531, test.getMilitaryId(), "Hiba! Nem egyezik az SZTSZ értéke!");
        assertTrue(test.getDateOfExecution().isEqual(LocalDate.of(2020, 5, 5)), "Hiba! Nem egyezik a végrehajtás ideje!");
        assertEquals(0, test.getArmFormOfMovement(), "Hiba! Nem egyezik a kar mozgásforma értéke!");
        assertEquals(50, test.getArmResult(), "Hiba! Nem egyezik a kar eredmény értéke!");
    }
    
    @Test
    public void constructorGetterTestTwo() {
        LocalDate dateOfExecution = LocalDate.of(2020, 5, 5);
        PhysicalTest test = new PhysicalTest(1, 50034531, dateOfExecution, 0, 50, 0, 60, 0, 1230);
        assertEquals(0, test.getTrunkFormOfMovement(), "Hiba! Nem egyezik a törzs mozgásforma értéke!");
        assertEquals(60, test.getTrunkResult(), "Hiba! Nem egyezik a törzs eredmény értéke!");
        assertEquals(0, test.getCirculationFormOfMovement(), "Hiba! Nem egyezik a keringés mozgásforma értéke!");
        assertEquals(1230, test.getCirculationResult(), "Hiba! Nem egyezik a keringés eredmény értéke!");
        assertEquals(0, test.getScore(), "Hiba! Nem egyezik a pontszám értéke!");
        assertNull(test.getBmi(), "Hiba! Nem null a bmi értéke!");
        assertNull(test.getBodyFatIndex(), "Hiba! Nem null a testzsír index értéke!");
    }
    
    @Test
    public void constructorGetterTestThree() {
        LocalDate dateOfExecution = LocalDate.of(2019, 4, 23);
        PhysicalTest test = new PhysicalTest(2, 50012345, dateOfExecution, 1, 78, 1, 16, 1, 1352, 300, "MF", "MF");
        assertEquals(Integer.valueOf(2), test.getId(), "Hiba! Nem egyezik a sorszám értéke!");
        assertEquals(50012345, test.getMilitaryId(), "Hiba! Nem egyezik az SZTSZ értéke!");
        assertTrue(test.getDateOfExecution().isEqual(LocalDate.of(2019, 4, 23)), "Hiba! Nem egyezik a végrehajtás ideje!");
        assertEquals(1, test.getArmFormOfMovement(), "Hiba! Nem egyezik a kar mozgásforma értéke!");
        assertEquals(78, test.getArmResult(), "Hiba! Nem egyezik a kar eredmény értéke!");
        
    }
    
    @Test
    public void constructorGetterTestFour() {
        LocalDate dateOfExecution = LocalDate.of(2019, 4, 23);
        PhysicalTest test = new PhysicalTest(2, 50012345, dateOfExecution, 1, 78, 1, 16, 1, 1352, 300, "MF", "MF");
        assertEquals(1, test.getTrunkFormOfMovement(), "Hiba! Nem egyezik a törzs mozgásforma értéke!");
        assertEquals(16, test.getTrunkResult(), "Hiba! Nem egyezik a törzs eredmény értéke!");
        assertEquals(1, test.getCirculationFormOfMovement(), "Hiba! Nem egyezik a keringés mozgásforma értéke!");
        assertEquals(1352, test.getCirculationResult(), "Hiba! Nem egyezik a keringés eredmény értéke!");
        assertEquals(300, test.getScore(), "Hiba! Nem egyezik a pontszám értéke!");
        assertEquals("MF", test.getBmi(), "Hiba! Nem egyezik a bmi értéke!");
        assertEquals("MF",  test.getBodyFatIndex(), "Hiba! Nem egyezik a testzsír index értéke!");
    }
    
    @Test
    public void constructorGetterTestFive() {
        PhysicalTest test = new PhysicalTest();
        assertNull(test.getId(), "Hiba! Nem null a sorszám értéke!");
        assertEquals(0, test.getMilitaryId(), "Hiba! Nem nulla az SZTSZ értéke!");
        assertNull(test.getDateOfExecution(), "Hiba! Nem null a végrehajtás ideje!");
        assertEquals(0, test.getArmFormOfMovement(), "Hiba! Nem nulla a kar mozgásforma értéke!");
        assertEquals(0, test.getArmResult(), "Hiba! Nem nulla a kar eredmény értéke!");
        assertEquals(0, test.getTrunkFormOfMovement(), "Hiba! Nem nulla a törzs mozgásforma értéke!");
        assertEquals(0, test.getTrunkResult(), "Hiba! Nem nulla a törzs eredmény értéke!");
        assertEquals(0, test.getCirculationFormOfMovement(), "Hiba! Nem nulla a keringés mozgásforma értéke!");
        assertEquals(0, test.getCirculationResult(), "Hiba! Nem nulla a keringés eredmény értéke!");
        assertEquals(0, test.getScore(), "Hiba! Nem nulla a pontszám értéke!");
        assertNull(test.getBmi(), "Hiba! Nem null a bmi értéke!");
        assertNull(test.getBodyFatIndex(), "Hiba! Nem null a testzsír index értéke!");
    }
    
    @Test
    public void constructorGetterSetterTestOne() {
        PhysicalTest test = new PhysicalTest();
        test.setId(3);
        test.setMilitaryId(29349876);
        test.setDateOfExecution(LocalDate.of(2019, 7, 2));
        test.setArmFormOfMovement(1);
        test.setArmResult(20);
        assertEquals(Integer.valueOf(3), test.getId(), "Hiba! Nem egyezik a sorszám értéke!");
        assertEquals(29349876, test.getMilitaryId(), "Hiba! Nem egyezik az SZTSZ értéke!");
        assertTrue(test.getDateOfExecution().isEqual(LocalDate.of(2019, 7, 2)), "Hiba! Nem egyezik a végrehajtás ideje!");
        assertEquals(1, test.getArmFormOfMovement(), "Hiba! Nem egyezik a kar mozgásforma értéke!");
        assertEquals(20, test.getArmResult(), "Hiba! Nem egyezik a kar eredmény értéke!");
    }
    
    @Test
    public void constructorGetterSetterTestTwo() {
        PhysicalTest test = new PhysicalTest();
        test.setTrunkFormOfMovement(1);
        test.setTrunkResult(47);
        test.setCirculationFormOfMovement(1);
        test.setCirculationResult(1534);
        test.setScore(333);
        test.setBmi("MF");
        test.setBodyFatIndex("NMF");
        assertEquals(1, test.getTrunkFormOfMovement(), "Hiba! Nem egyezik a törzs mozgásforma értéke!");
        assertEquals(47, test.getTrunkResult(), "Hiba! Nem egyezik a törzs eredmény értéke!");
        assertEquals(1, test.getCirculationFormOfMovement(), "Hiba! Nem egyezik a keringés mozgásforma értéke!");
        assertEquals(1534, test.getCirculationResult(), "Hiba! Nem egyezik a keringés eredmény értéke!");
        assertEquals(333, test.getScore(), "Hiba! Nem egyezik a pontszám értéke!");
        assertEquals("MF", test.getBmi(), "Hiba! Nem egyezik a bmi értéke!");
        assertEquals("NMF",  test.getBodyFatIndex(), "Hiba! Nem egyezik a testzsír index értéke!");
    }
    
    @Test
    public void constructorGetterSetterTestThree() {
        LocalDate dateOfExecution = LocalDate.of(2020, 10, 19);
        PhysicalTest test = new PhysicalTest(4, 50024689, dateOfExecution, 2, 45, 2, 81, 2, 1316);
        test.setId(5);
        test.setMilitaryId(12345678);
        test.setDateOfExecution(LocalDate.of(2018, 9, 30));
        test.setArmFormOfMovement(0);
        test.setArmResult(66);
        assertEquals(Integer.valueOf(5), test.getId(), "Hiba! Nem egyezik a sorszám értéke!");
        assertEquals(12345678, test.getMilitaryId(), "Hiba! Nem egyezik az SZTSZ értéke!");
        assertTrue(test.getDateOfExecution().isEqual(LocalDate.of(2018, 9, 30)), "Hiba! Nem egyezik a végrehajtás ideje!");
        assertEquals(0, test.getArmFormOfMovement(), "Hiba! Nem egyezik a kar mozgásforma értéke!");
        assertEquals(66, test.getArmResult(), "Hiba! Nem egyezik a kar eredmény értéke!");
    }
    
    @Test
    public void constructorGetterSetterTestFour() {
        LocalDate dateOfExecution = LocalDate.of(2020, 10, 19);
        PhysicalTest test = new PhysicalTest(4, 50024689, dateOfExecution, 2, 45, 2, 81, 2, 1316);
        test.setTrunkFormOfMovement(0);
        test.setTrunkResult(100);
        test.setCirculationFormOfMovement(3);
        test.setCirculationResult(322);
        test.setScore(279);
        test.setBmi("NMF");
        test.setBodyFatIndex("NMF");
        assertEquals(0, test.getTrunkFormOfMovement(), "Hiba! Nem egyezik a törzs mozgásforma értéke!");
        assertEquals(100, test.getTrunkResult(), "Hiba! Nem egyezik a törzs eredmény értéke!");
        assertEquals(3, test.getCirculationFormOfMovement(), "Hiba! Nem egyezik a keringés mozgásforma értéke!");
        assertEquals(322, test.getCirculationResult(), "Hiba! Nem egyezik a keringés eredmény értéke!");
        assertEquals(279, test.getScore(), "Hiba! Nem egyezik a pontszám értéke!");
        assertEquals("NMF", test.getBmi(), "Hiba! Nem egyezik a bmi értéke!");
        assertEquals("NMF",  test.getBodyFatIndex(), "Hiba! Nem egyezik a testzsír index értéke!");
    }

}
