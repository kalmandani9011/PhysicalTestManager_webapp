package physicaltestmanager.model.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.mycompany.physicaltestmanager.model.Person;
import com.mycompany.physicaltestmanager.model.PhysicalTest;
import com.mycompany.physicaltestmanager.model.Rank;

class PersonTest {

	@Test
    public void constructorGetterTestOne() {
        Integer id = 1;
        int militaryId = 50012345;
        String name = "Rogal Dorn";
        Rank rank = Rank.VEZEREZREDES;
        String unit = "BOCSKAI";
        LocalDate birthDate = LocalDate.of(1940, 11, 30);
        Person person = new Person(id, militaryId, name, rank, unit, birthDate, "", true, 250, 200, 10);
        assertEquals(Integer.valueOf(1), person.getId(), "Hiba! Nem egyezik a sorszám értéke!");
        assertEquals(50012345, person.getMilitaryId(), "Hiba! Nem egyezik az SZTSZ értéke!");
        assertTrue(person.getName().equals("Rogal Dorn"), "Hiba! Nem egyezik a név!");
        assertTrue(person.getRank().equals(Rank.VEZEREZREDES), "Hiba! Nem egyezik a rendfokozat!");
        assertTrue(person.getUnit().equals("BOCSKAI"), "Hiba! Nem egyezik az alakulat!");
        assertTrue(person.getBirthDate().isEqual(LocalDate.of(1940, 11, 30)), "Hiba! Nem egyezik a születési dátum!");
        assertEquals("1   | 50012345 | Rogal Dorn vezérezredes                 ", person.toString(), "Hiba! Nem egyezik a toString() metódus visszatérési értéke!");
    }
    
    @Test
    public void constructorGetterTestTwo() {
        Rank rank = Rank.VEZEREZREDES;
        String unit = "BOCSKAI";
        LocalDate birthDate = LocalDate.of(1940, 11, 30);
        String mothersMaidenName = "Princess Leia";
        boolean male = true;
        int height = 250;
        int weight = 200;
        double bodyFat = 10.4;
        Person person = new Person(1, 50012345, "Rogal Dorn", rank, unit, birthDate, mothersMaidenName, male, height, weight, bodyFat);
        assertTrue(person.getMothersMaidenName().equals("Princess Leia"), "Hiba! Nem egyezik az anyja neve!");
        assertTrue(person.isMale(), "Hiba! Nem egyezik a férfi/nő értéke!");
        assertEquals(250, person.getHeight(), "Hiba! Nem egyezik a magasság!");
        assertEquals(200, person.getWeight(), "Hiba! Nem egyezik a súly!");
        assertEquals(10.4, person.getBodyFat(), 0, "Hiba! Nem egyezik a test zsír értéke!");
        assertEquals(82, person.getAge(), "Hiba! Nem egyezik a kor!");
        assertTrue(person.getTests().isEmpty(), "Hiba! Nem üres a felmérések listája!");
    }
    
    @Test
    public void constructorGetterTestThree() {
        Rank rank = Rank.ORMESTER;
        LocalDate birthDate = LocalDate.of(1995, 10, 11);
        Person person = new Person(50054321, "Lukács Adrienn", rank, "KLAPKA", birthDate, "Nagy Mónika", false, 167, 53, 13.9);
        assertNull(person.getId(), "Hiba! Nem null a sorszám értéke!");
        assertEquals(50054321, person.getMilitaryId(), "Hiba! Nem egyezik az SZTSZ értéke!");
        assertTrue(person.getName().equals("Lukács Adrienn"), "Hiba! Nem egyezik a név!");
        assertTrue(person.getRank().equals(Rank.ORMESTER), "Hiba! Nem egyezik a rendfokozat!");
        assertTrue(person.getUnit().equals("KLAPKA"), "Hiba! Nem egyezik az alakulat!");
        assertTrue(person.getBirthDate().isEqual(LocalDate.of(1995, 10, 11)), "Hiba! Nem egyezik a születési dátum!");
        assertTrue(person.getMothersMaidenName().equals("Nagy Mónika"), "Hiba! Nem egyezik az anyja neve!");
        assertFalse(person.isMale(), "Hiba! Nem egyezik a férfi/nő értéke!");
        assertEquals(167, person.getHeight(), "Hiba! Nem egyezik a magasság!");
        assertEquals(53, person.getWeight(), "Hiba! Nem egyezik a súly!");
        assertEquals(13.9, person.getBodyFat(), 0, "Hiba! Nem egyezik a test zsír értéke!");
        assertEquals(27, person.getAge(), "Hiba! Nem egyezik a kor!");
        assertTrue(person.getTests().isEmpty(), "Hiba! Nem üres a felmérések listája!");
        assertEquals("null| 50054321 | Lukács Adrienn őrmester                 ", person.toString(), "Hiba! Nem egyezik a toString() metódus visszatérési értéke!");
    }
    
    @Test
    public void constructorGetterTestFour() {
        Person person = new Person();
        assertNull(person.getId(), "Hiba! Nem null a sorszám értéke!");
        assertEquals(0, person.getMilitaryId(), "Hiba! Nem nulla az SZTSZ értéke!");
        assertNull(person.getName(), "Hiba! Nem null a név értéke!");
        assertNull(person.getRank(), "Hiba! Nem null a rendfokozat értéke!");
        assertNull(person.getUnit(), "Hiba! Nem null az alakulat értéke!");
        assertNull(person.getBirthDate(), "Hiba! Nem null a születési dátum értéke!");
        assertNull(person.getMothersMaidenName(), "Hiba! Nem null az anyja neve értéke!");
        assertFalse(person.isMale(), "Hiba! Nem false a férfi/nő értéke!");
        assertEquals(0, person.getHeight(), "Hiba! Nem nulla a magasság értéke!");
        assertEquals(0, person.getWeight(), "Hiba! Nem nulla a súly értéke!");
        assertEquals(0, person.getBodyFat(), 0, "Hiba! Nem nulla a test zsír értéke!");
        assertEquals(0, person.getAge(), "Hiba! Nem nulla a kor értéke!");
        assertNull(person.getTests(), "Hiba! Nem null a felmérések listája!");
        assertEquals("", person.toString(), "Hiba! Nem egyezik a toString() metódus visszatérési értéke!");
    }
    
    @Test
    public void constructorSetterGetterTestOne() {
        Person person = new Person();
        person.setId(2);
        person.setMilitaryId(12345678);
        person.setName("Tóth Tibor");
        person.setRank(Rank.ORVEZETO);
        person.setUnit("KLAPKA");
        person.setBirthDate(LocalDate.of(1997, 5, 18));
        assertEquals(Integer.valueOf(2), person.getId(), "Hiba! Nem egyezik a sorszám értéke!");
        assertEquals(12345678, person.getMilitaryId(), "Hiba! Nem egyezik az SZTSZ értéke!");
        assertTrue(person.getName().equals("Tóth Tibor"), "Hiba! Nem egyezik a név!");
        assertTrue(person.getRank().equals(Rank.ORVEZETO), "Hiba! Nem egyezik a rendfokozat!");
        assertTrue(person.getUnit().equals("KLAPKA"), "Hiba! Nem egyezik az alakulat!");
        assertTrue(person.getBirthDate().isEqual(LocalDate.of(1997, 5, 18)), "Hiba! Nem egyezik a születési dátum!");
        assertEquals(25, person.getAge(), "Hiba! Nem egyezik a kor!");
        assertEquals("2   | 12345678 | Tóth Tibor őrvezető                     ", person.toString(), "Hiba! Nem egyezik a toString() metódus visszatérési értéke!");
    }
    
    @Test
    public void constructorSetterGetterTestTwo() {
        Person person = new Person();
        person.setMothersMaidenName("Ordas Emese");
        person.setMale(true);
        person.setHeight(182);
        person.setWeight(78);
        person.setBodyFat(6.7);
        assertTrue(person.getMothersMaidenName().equals("Ordas Emese"), "Hiba! Nem egyezik az anyja neve!");
        assertTrue(person.isMale(), "Hiba! Nem egyezik a férfi/nő értéke!");
        assertEquals(182, person.getHeight(), "Hiba! Nem egyezik a magasság!");
        assertEquals(78, person.getWeight(), "Hiba! Nem egyezik a súly!");
        assertEquals(6.7, person.getBodyFat(), 0, "Hiba! Nem egyezik a test zsír értéke!");
        assertNull(person.getTests(), "Hiba! Nem null a felmérések listája!");
    }
    
    @Test
    public void constructorSetterGetterTestThree() {
        Rank rank = Rank.ORMESTER;
        String unit = "BOCSKAI";
        LocalDate birthDate = LocalDate.of(1992, 2, 5);
        PhysicalTest test = new PhysicalTest();
        PhysicalTest test2 = new PhysicalTest();
        List<PhysicalTest> testList = new ArrayList<>();
        testList.add(test);
        testList.add(test2);
        Person person = new Person(3, 43215678, "Ambrus Levente", rank, unit, birthDate, "Kiss Anna", true, 177, 72, 11);
        person.setId(4);
        person.setMilitaryId(98765432);
        person.setName("Fekete Renáta");
        person.setRank(Rank.FOTORZSORMESTER);
        person.setTests(testList);
        assertEquals(Integer.valueOf(4), person.getId(), "Hiba! Nem egyezik a sorszám értéke!");
        assertEquals(98765432, person.getMilitaryId(), "Hiba! Nem egyezik az SZTSZ értéke!");
        assertTrue(person.getName().equals("Fekete Renáta"), "Hiba! Nem egyezik a név!");
        assertTrue(person.getRank().equals(Rank.FOTORZSORMESTER), "Hiba! Nem egyezik a rendfokozat!");
        assertFalse(person.getTests().isEmpty(), "Hiba! Nem lehet üres a felmérések listája!");
        assertEquals(2, person.getTests().size(), "Hiba! Nem egyezik a felmérések száma!");
        assertEquals("4   | 98765432 | Fekete Renáta főtörzsőrmester           ", person.toString(), "Hiba! Nem egyezik a toString() metódus visszatérési értéke!");
    }
    
    @Test
    public void constructorSetterGetterTestFour() {
        Rank rank = Rank.ORMESTER;
        String unit = "BOCSKAI";
        LocalDate birthDate = LocalDate.of(1992, 2, 5);
        Person person = new Person(3, 43215678, "Ambrus Levente", rank, unit, birthDate, "Kiss Anna", true, 177, 72, 11);
        person.setUnit("KLAPKA");
        person.setBirthDate(LocalDate.of(1988, 9, 23));
        person.setMothersMaidenName("Vígh Melinda");
        person.setMale(false);
        person.setHeight(163);
        person.setWeight(50);
        person.setBodyFat(13.2);
        assertTrue(person.getUnit().equals("KLAPKA"), "Hiba! Nem egyezik az alakulat!");
        assertTrue(person.getBirthDate().isEqual(LocalDate.of(1988, 9, 23)), "Hiba! Nem egyezik a születési dátum!");
        assertTrue(person.getMothersMaidenName().equals("Vígh Melinda"), "Hiba! Nem egyezik az anyja neve!");
        assertFalse(person.isMale(), "Hiba! Nem egyezik a férfi/nő értéke!");
        assertEquals(163, person.getHeight(), "Hiba! Nem egyezik a magasság!");
        assertEquals(50, person.getWeight(), "Hiba! Nem egyezik a súly!");
        assertEquals(13.2, person.getBodyFat(), 0, "Hiba! Nem egyezik a test zsír értéke!");
        assertEquals(34, person.getAge(), "Hiba! Nem egyezik a kor!");
    }

}
