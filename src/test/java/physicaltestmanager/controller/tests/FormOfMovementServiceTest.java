package physicaltestmanager.controller.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.mycompany.physicaltestmanager.exception.PhysicalTestManagerDAOException;
import com.mycompany.physicaltestmanager.model.Person;
import com.mycompany.physicaltestmanager.model.PhysicalTest;
import com.mycompany.physicaltestmanager.service.FormOfMovementService;
import com.mycompany.physicaltestmanager.util.DataSourceUtil;

class FormOfMovementServiceTest {

	 	private static FormOfMovementService movementService;
	    private Person testPerson;
	    
	    @BeforeAll
	    public static void setUpClass() throws SQLException {
	        try {
	        	String datasourceUrl = "jdbc:mysql://localhost:3306/physical_test_table?serverTimezone=UTC";
	        	String datasourceDriver = "com.mysql.cj.jdbc.Driver";
	        	String datasourceUsername = "root";
	        	String datasourcePassword = "1234";
	            DataSourceUtil dsUtil = DataSourceUtil.getInstance(datasourceUrl, datasourceDriver, datasourceUsername, datasourcePassword);
	            movementService = new FormOfMovementService(dsUtil);
	        } catch (PhysicalTestManagerDAOException ex) {
	            ex.printStackTrace();
	        }
	    }
	    
	    @BeforeEach
	    public void setUp() {
	        testPerson = new Person();
	        testPerson.setMale(true);
	        testPerson.setBodyFat(7.9);
	        testPerson.setBirthDate(LocalDate.of(2000, 3, 3));
	        testPerson.setHeight(180);
	        testPerson.setWeight(90);
	    }
	    
	    @Test
	    public void bodyFatIndexTest() {
	        String index = movementService.determineBodyFat(testPerson);
	        assertEquals("NMF", index);
	        
	        testPerson.setBodyFat(8.0);
	        index = movementService.determineBodyFat(testPerson);
	        assertEquals("MF", index);
	        
	        testPerson.setBodyFat(15.9);
	        index = movementService.determineBodyFat(testPerson);
	        assertEquals("MF", index);
	        
	        testPerson.setBodyFat(17.6);
	        index = movementService.determineBodyFat(testPerson);
	        assertEquals("NMF", index);
	        
	        testPerson.setMale(false);
	        testPerson.setBodyFat(19.9);
	        index = movementService.determineBodyFat(testPerson);
	        assertEquals("NMF", index);
	            
	        testPerson.setBodyFat(30.0);
	        index = movementService.determineBodyFat(testPerson);
	        assertEquals("MF", index);
	    }

	    @Test
	    public void bmiIndexTest() {
	        try {
	            String bmi = movementService.determineBmi(testPerson);
	            assertEquals("NMF", bmi);
	            
	            testPerson.setWeight(89);
	            bmi = movementService.determineBmi(testPerson);
	            assertEquals("MF", bmi);
	            
	            testPerson.setHeight(190);
	            testPerson.setWeight(63);
	            bmi = movementService.determineBmi(testPerson);
	            assertEquals("NMF", bmi);
	            
	            testPerson.setWeight(64);
	            bmi = movementService.determineBmi(testPerson);
	            assertEquals("MF", bmi);
	            
	            testPerson.setWeight(118);
	            testPerson.setMale(false);
	            testPerson.setBirthDate(LocalDate.of(1975, 1, 1));
	            bmi = movementService.determineBmi(testPerson);
	            assertEquals("NMF", bmi);
	            
	            testPerson.setWeight(117);
	            bmi = movementService.determineBmi(testPerson);
	            assertEquals("MF", bmi);
	            
	            testPerson.setBirthDate(LocalDate.of(1976, 1, 1));
	            bmi = movementService.determineBmi(testPerson);
	            assertEquals("MF", bmi);
	            
	            testPerson.setWeight(111);
	            bmi = movementService.determineBmi(testPerson);
	            assertEquals("MF", bmi);
	        } catch (PhysicalTestManagerDAOException ex) {
	            ex.printStackTrace();
	        }
	    }

	    @Test
	    public void getScoreTest() {
	        try {
	            int armType = 0;
	            int armScore = 69;
	            int trunkType = 0;
	            int trunkScore = 84;
	            int circulationType = 0;
	            int circulationScore = 1340;
	            PhysicalTest test = new PhysicalTest(0, LocalDate.now(), armType, armScore, trunkType, trunkScore,
	                    circulationType, circulationScore);
	            int score = movementService.getScore(testPerson, test);
	            assertEquals(350, score);
	            assertEquals("MF", movementService.getQualification());
	            
	            armScore = 76;
	            trunkScore = 90;
	            circulationScore = 1310;
	            test.setArmResult(armScore);
	            test.setTrunkResult(trunkScore);
	            test.setCirculationResult(circulationScore);
	            score = movementService.getScore(testPerson, test);
	            assertEquals(360, score);
	            assertEquals("MF", movementService.getQualification());
	            
	            armScore = 13;
	            test.setArmResult(armScore);
	            score = movementService.getScore(testPerson, test);
	            assertEquals(260, score);
	            assertEquals("NMF", movementService.getQualification());
	            
	            testPerson.setBirthDate(LocalDate.of(1975, 3, 3));
	            armType = 1;
	            trunkType = 1;
	            circulationType = 1;
	            armScore = 19;
	            trunkScore = 72;
	            circulationScore = 1040;
	            test.setArmFormOfMovement(armType);
	            test.setTrunkFormOfMovement(trunkType);
	            test.setCirculationFormOfMovement(circulationType);
	            test.setArmResult(armScore);
	            test.setTrunkResult(trunkScore);
	            test.setCirculationResult(circulationScore);
	            score = movementService.getScore(testPerson, test);
	            assertEquals(326, score);
	            assertEquals("MF", movementService.getQualification());
	            
	            testPerson.setMale(false);
	            armType = 2;
	            trunkType = 2;
	            circulationType = 2;
	            armScore = 25;
	            trunkScore = 31;
	            circulationScore = 1535;
	            test.setArmFormOfMovement(armType);
	            test.setTrunkFormOfMovement(trunkType);
	            test.setCirculationFormOfMovement(circulationType);
	            test.setArmResult(armScore);
	            test.setTrunkResult(trunkScore);
	            test.setCirculationResult(circulationScore);
	            score = movementService.getScore(testPerson, test);
	            assertEquals(257, score);
	            assertEquals("MF", movementService.getQualification());
	            
	            circulationType = 3;
	            circulationScore = 111;
	            test.setCirculationFormOfMovement(circulationType);
	            test.setCirculationResult(circulationScore);
	            score = movementService.getScore(testPerson, test);
	            assertEquals(152, score);
	            assertEquals("NMF", movementService.getQualification());
	        } catch (PhysicalTestManagerDAOException ex) {
	            ex.printStackTrace();
	        }
	    }

}
