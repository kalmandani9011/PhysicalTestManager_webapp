package physicaltestmanager.person.dao.impl.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.mycompany.physicaltestmanager.exception.PhysicalTestManagerDAOException;
import com.mycompany.physicaltestmanager.model.Person;
import com.mycompany.physicaltestmanager.model.Rank;
import com.mycompany.physicaltestmanager.person.dao.impl.PersonRepositoryJDBCImpl;
import com.mycompany.physicaltestmanager.util.DataSourceUtil;

class PersonRepositoryJDBCImplTest {

	private static PersonRepositoryJDBCImpl jdbc;
    private static Person testPerson;
    
    @BeforeAll
    public static void setUpClass() throws SQLException {
        try {
        	String datasourceUrl = "jdbc:mysql://localhost:3306/physical_test_table?serverTimezone=UTC";
        	String datasourceDriver = "com.mysql.cj.jdbc.Driver";
        	String datasourceUsername = "root";
        	String datasourcePassword = "1234";
            DataSourceUtil dsUtil = DataSourceUtil.getInstance(datasourceUrl, datasourceDriver, datasourceUsername, datasourcePassword);
            jdbc = new PersonRepositoryJDBCImpl(dsUtil);
            int militaryId = 98765432;
            String name = "Test Test";
            Rank rank = Rank.ORNAGY;
            String unit = "BOCSKAI";
            LocalDate birthDate = LocalDate.of(2000, 1, 1);
            String mothersMaidenName = "Test Mother";
            boolean male = true;
            int height = 190;
            int weight = 95;
            double bodyFat = 10.5;
            testPerson = new Person(militaryId, name, rank, unit, birthDate, mothersMaidenName, male, height, weight, bodyFat);
            assertTrue(testPerson.getId() == null, "Nem null-t állít be sorszámnak!");
            jdbc.savePerson(testPerson);
        } catch (PhysicalTestManagerDAOException ex) {
            ex.printStackTrace();
        }
    }
    
    @AfterAll
    public static void tearDownClass() {
        try {
            Person deletePersonTest = jdbc.findByMilitaryId(98765432);
            jdbc.deletePerson(deletePersonTest.getId());
            deletePersonTest = jdbc.findByMilitaryId(98765432);
            assertTrue(deletePersonTest == null, "A deleteSzemely metódus nem működik jól: szerepel a teszt személy az adatbázisban törlés után!");
        } catch (PhysicalTestManagerDAOException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void findByNamePersonTest() {
        try {
            List<Person> findByNameTest = jdbc.findByName("Test Test");
            assertTrue(findByNameTest.size() == 1, "Nem egy személyt ad hozzá az adatbázishoz!");
            Person findByNamePersonTest = findByNameTest.get(0);
            assertTrue(findByNamePersonTest != null, "Nincs az adatbázisban a teszt személy!");
            assertTrue(findByNamePersonTest.getName().equals("Test Test"), "Nincs az adatbázisban a teszt személy!");
            assertTrue(findByNamePersonTest.getId() != null, "Az adatbázis nem ad neki sorszámot!");
        } catch (PhysicalTestManagerDAOException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void findByMilitaryIdTest() {
        try {
            Person findByMilitaryIdTest = jdbc.findByMilitaryId(98765432);
            assertTrue(findByMilitaryIdTest.getMilitaryId() == 98765432, "Nincs az adatbázisban a teszt személy!");
        } catch (PhysicalTestManagerDAOException ex) {
            ex.printStackTrace();
        } 
    }
    
    @Test
    public void updatePersonTest() {
        try {
            Person testPerson = jdbc.findByMilitaryId(98765432);
            assertTrue(testPerson.getRank().equals(Rank.ORNAGY), "Nincsenek jól beállítva az adatok, vagy nem működik az insert metódus!");
            assertTrue(testPerson.isMale(), "Nincsenek jól beállítva az adatok, vagy nem működik az insert metódus!");
            testPerson.setRank(Rank.ALEZREDES);
            testPerson.setMale(false);
            jdbc.updatePerson(testPerson);
            assertTrue(testPerson.getRank().equals(Rank.ALEZREDES), "Nem frissíti a személy adatait!");
            assertFalse(testPerson.isMale(), "Nem frissíti a személy adatait!");
        } catch (PhysicalTestManagerDAOException ex) {
            ex.printStackTrace();
        }
    }

}
