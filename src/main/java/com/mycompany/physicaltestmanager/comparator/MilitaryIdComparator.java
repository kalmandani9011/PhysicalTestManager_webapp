package com.mycompany.physicaltestmanager.comparator;

import java.util.Comparator;

import com.mycompany.physicaltestmanager.model.Person;

/**
 *
 * @author
 */
public class MilitaryIdComparator implements Comparator<Person> {
    
    @Override
    public int compare(Person sz, Person sz2) {
        return sz.getMilitaryId() - sz2.getMilitaryId();
    }
}
