package com.mycompany.physicaltestmanager.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mycompany.physicaltestmanager.exception.PhysicalTestManagerDAOException;
import com.mycompany.physicaltestmanager.model.Person;
import com.mycompany.physicaltestmanager.service.ScoreCalculatorService;

@Controller
@RequestMapping("/person")
public class PersonController {

	@Autowired
	private ScoreCalculatorService calculator;
	private Logger log = LoggerFactory.getLogger(PersonController.class);
	
	@PostMapping
	public String person(Model model, @RequestParam("selectedPerson") String selectedPerson, 
			@RequestParam(value="action", required=true) String action) {
		try {
			switch (action) {
				case "update":
					Person person = calculator.findPersonById(Integer.parseInt(selectedPerson));
					model.addAttribute("person", person);
					model.addAttribute("page_title", person.getName() + " adatainak módosítása");
					return "person";
				case "delete":
					person = calculator.findPersonById(Integer.parseInt(selectedPerson));
					model.addAttribute("person", person);
					model.addAttribute("page_title", person.getName() + " törlése");
					return "confirmation";
				case "testOverview":
					person = calculator.findPersonById(Integer.parseInt(selectedPerson));
					return "redirect:/test/overview?militaryId=" + person.getMilitaryId();
			}
		} catch (PhysicalTestManagerDAOException ex) {
			log.error(ex.getMessage(), ex.fillInStackTrace());
		}
		return "redirect:/display";
	}
	
	@GetMapping("/new")
	public String newPerson(Model model) {
		model.addAttribute("person", new Person());
		model.addAttribute("page_title", "Új személy hozzáadása");
		return "person";
	}
	
	@PostMapping("/save")
	public String savePerson(Person person) {
		try {
			calculator.savePerson(person);
		} catch (PhysicalTestManagerDAOException ex) {
			log.error(ex.getMessage(), ex.fillInStackTrace());
		}
		return "redirect:/display";
	}
	
	@PostMapping("/delete")
	public String deletePerson(Person person) {
		try {
			calculator.deletePerson(person.getId());
		} catch (PhysicalTestManagerDAOException ex) {
			log.error(ex.getMessage(), ex.fillInStackTrace());
		}
		return "redirect:/display";
	}
}
