package com.mycompany.physicaltestmanager.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.mycompany.physicaltestmanager.exception.PhysicalTestManagerDAOException;
import com.mycompany.physicaltestmanager.model.PersonTestDTO;
import com.mycompany.physicaltestmanager.service.ScoreCalculatorService;

@Controller
public class MainController {
	
	@Autowired
	private ScoreCalculatorService calculator;
	private Logger log = LoggerFactory.getLogger(MainController.class);
	
	@GetMapping("/display")
	public String display(Model model) {
		try {
			model.addAttribute("persons", calculator.getPersons());
		} catch (PhysicalTestManagerDAOException ex) {
			log.error(ex.getMessage(), ex.fillInStackTrace());
		}
		return "display";
	}
	
	@GetMapping("/calculator")
	public String calculator(Model model) {
		model.addAttribute("personTestDTO", new PersonTestDTO());
		return "calculator";
	}
	
	@PostMapping("/calculate")
	public String calculate(Model model, PersonTestDTO personTestDTO) {
		try {
			personTestDTO.getPerson().setHeight(170);
			calculator.calculateTestScore(personTestDTO.getPerson(), personTestDTO.getTest());
			model.addAttribute("personTestDTO", personTestDTO);
		} catch (PhysicalTestManagerDAOException ex) {
			log.error(ex.getMessage(), ex.fillInStackTrace());
		}
		return "calculator";
	}
	
	@GetMapping("/tests")
	public String tests(Model model) {
		try {
			model.addAttribute("hasAnyTests", !calculator.getTests().isEmpty());
			model.addAttribute("persons", calculator.getPersonsWithTests());
		} catch (PhysicalTestManagerDAOException ex) {
			log.error(ex.getMessage(), ex.fillInStackTrace());
		}
		return "tests";
	}
}
