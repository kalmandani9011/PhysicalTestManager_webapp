package com.mycompany.physicaltestmanager.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mycompany.physicaltestmanager.exception.PhysicalTestManagerDAOException;
import com.mycompany.physicaltestmanager.model.Person;
import com.mycompany.physicaltestmanager.model.PhysicalTest;
import com.mycompany.physicaltestmanager.service.ScoreCalculatorService;

@Controller
@RequestMapping("/test")
public class PhysicalTestController {

	@Autowired
	private ScoreCalculatorService calculator;
	private Logger log = LoggerFactory.getLogger(PhysicalTestController.class);
	
	@GetMapping("/overview")
	public String testOverview(Model model, @RequestParam("militaryId") String militaryId) {
		try {
			Person person = calculator.findPersonByMilitaryId(Integer.parseInt(militaryId));
			List<PhysicalTest> tests = calculator.findTestByMilitaryId(person.getMilitaryId());
			model.addAttribute("tests", tests);
			model.addAttribute("person", person);
			model.addAttribute("page_title", person.getName() + " fizikai felmérései");
		} catch (PhysicalTestManagerDAOException | NumberFormatException ex) {
			log.error(ex.getMessage(), ex.fillInStackTrace());
		}
		return "test_overview";
	}
	
	@PostMapping
	public String test(Model model, @RequestParam("militaryId") String militaryId, @RequestParam("selectedTest") String selectedTest,
			@RequestParam(value="action", required=true) String action) {
		try {
			switch (action) {
				case "update":
					PhysicalTest test = calculator.findTestById(Integer.parseInt(selectedTest));
					model.addAttribute("test", test);
					model.addAttribute("page_title", "Fizikai felmérés adatainak módosítása");
					return "test";
				case "delete":
					test = calculator.findTestById(Integer.parseInt(selectedTest));
					model.addAttribute("test", test);
					model.addAttribute("page_title", "Fizikai felmérés törlése");
					return "testDeleteConf";
			}
		} catch (PhysicalTestManagerDAOException ex) {
			log.error(ex.getMessage(), ex.fillInStackTrace());
		}
		return "redirect:/test/overview";
	}
	
	@GetMapping("/new")
	public String newTest(Model model, @RequestParam("militaryId") String militaryId) {
		PhysicalTest test = new PhysicalTest();
		test.setMilitaryId(Integer.parseInt(militaryId));
		model.addAttribute("test", test);
		model.addAttribute("page_title", "Új fizikai felmérés hozzáadása");
		return "test";
	}
	
	@PostMapping("/save")
	public String saveTest(PhysicalTest test) {
		Person person;
		int militaryId = 0;
		try {
			person = calculator.findPersonByMilitaryId(test.getMilitaryId());
			calculator.saveTestAndBindToPerson(person, test);
			militaryId = person.getMilitaryId();
		} catch (PhysicalTestManagerDAOException ex) {
			log.error(ex.getMessage(), ex.fillInStackTrace());
		}
		return "redirect:/test/overview?militaryId=" + militaryId;
	}
	
	@PostMapping("/delete")
	public String deleteTest(PhysicalTest test) {
		int militaryId = 0;
		try {
			calculator.deleteTest(test.getId());
			militaryId = test.getMilitaryId();
		} catch (PhysicalTestManagerDAOException ex) {
			log.error(ex.getMessage(), ex.fillInStackTrace());
		}
		return "redirect:/test/overview?militaryId=" + militaryId;
	}
}
