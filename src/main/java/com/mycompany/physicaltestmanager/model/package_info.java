
/**
 * Ez a csomag tartalmazza az modellosztályokat: összes mozgásforma osztályát, enumokat, illetve a személy és fizikai állapot felmérés osztályait.
 */
package com.mycompany.physicaltestmanager.model;