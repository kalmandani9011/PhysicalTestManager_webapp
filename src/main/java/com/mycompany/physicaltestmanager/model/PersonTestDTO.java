package com.mycompany.physicaltestmanager.model;

import java.time.LocalDate;

public class PersonTestDTO {

	private Person person;
	private PhysicalTest test;
	private int personAge;
	
	public PersonTestDTO() {}
	
	public PersonTestDTO(Person person, PhysicalTest test, int personAge) {
		this.person = person;
		this.test = test;
		setPersonAge(personAge);
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public PhysicalTest getTest() {
		return test;
	}

	public void setTest(PhysicalTest test) {
		this.test = test;
	}

	public int getPersonAge() {
		if (person != null )
			return person.getAge();
		else
			return personAge;
	}

	public void setPersonAge(int personAge) {
		this.personAge = personAge;
		LocalDate temp = LocalDate.now().minusYears(personAge);
		this.person.setBirthDate(temp);
	}
}
