package com.mycompany.physicaltestmanager.model;

/**
 *A függő térdemelés mozgásforma osztálya.
 */
public class KneeRaise extends TrunkFormOfMovement {

    public KneeRaise(){}

	public KneeRaise(int repetition, int maleAgeGroupOne, int femaleAgeGroupOne, int maleAgeGroupTwo,
			int femaleAgeGroupTwo, int maleAgeGroupThree, int femaleAgeGroupThree, int maleAgeGroupFour,
			int femaleAgeGroupFour, int maleAgeGroupFive, int femaleAgeGroupFive, int maleAgeGroupSix,
			int femaleAgeGroupSix, int maleAgeGroupSeven, int femaleAgeGroupSeven, int maleAgeGroupEight,
			int femaleAgeGroupEigth, int maleAgeGroupNine, int femaleAgeGroupNine) {
		super(repetition, maleAgeGroupOne, femaleAgeGroupOne, maleAgeGroupTwo, femaleAgeGroupTwo, maleAgeGroupThree,
				femaleAgeGroupThree, maleAgeGroupFour, femaleAgeGroupFour, maleAgeGroupFive, femaleAgeGroupFive,
				maleAgeGroupSix, femaleAgeGroupSix, maleAgeGroupSeven, femaleAgeGroupSeven, maleAgeGroupEight,
				femaleAgeGroupEigth, maleAgeGroupNine, femaleAgeGroupNine);
	}
}