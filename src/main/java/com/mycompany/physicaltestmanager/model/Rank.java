package com.mycompany.physicaltestmanager.model;

/**
 *A Magyar Honvédségben használatos rendfokozatok.
 */
public enum Rank {
    
    KOZKATONA("kk."){
        @Override
        public String getFullName() {
            return "közkatona";
        }
    }, ORVEZETO("őrv."){
        @Override
        public String getFullName() {
            return "őrvezető";
        }
    }, TIZEDES("tiz."){
        @Override
        public String getFullName() {
            return "tizedes";
        }
    }, SZAKASZVEZETO("szkv."){
        @Override
        public String getFullName() {
            return "szakaszvezető";
        }
    }, ORMESTER("őrm."){
        @Override
        public String getFullName() {
            return "őrmester";
        }
    }, TORZSORMESTER("tőrm."){
        @Override
        public String getFullName() {
            return "törzsőrmester";
        }
    }, FOTORZSORMESTER("ftőrm."){
        @Override
        public String getFullName() {
            return "főtörzsőrmester";
        }
    }, ZASZLOS("zls."){
        @Override
        public String getFullName() {
            return "zászlós";
        }
    }, TORZSZASZLOS("tzls."){
        @Override
        public String getFullName() {
            return "törzszászlós";
        }
    }, FOTORZSZASZLOS("ftzls."){
        @Override
        public String getFullName() {
            return "főtörzszászlós";
        }
    }, HADNAGY("hdgy."){
        @Override
        public String getFullName() {
            return "hadnagy";
        }
    }, FOHADNAGY("fhdgy."){
        @Override
        public String getFullName() {
            return "főhadnagy";
        }
    }, SZAZADOS("szds."){
        @Override
        public String getFullName() {
            return "százados";
        }
    }, ORNAGY("őrgy."){
        @Override
        public String getFullName() {
            return "őrnagy";
        }
    }, ALEZREDES("alez."){
        @Override
        public String getFullName() {
            return "alezredes";
        }
    }, EZREDES("ezds."){
        @Override
        public String getFullName() {
            return "ezredes";
        }
    }, DANDARTABORNOK("ddtbk."){
        @Override
        public String getFullName() {
            return "dandártábornok";
        }
    }, VEZERORNAGY("vőrgy."){
        @Override
        public String getFullName() {
            return "vezérőrnagy";
        }
    }, ALTABORNAGY("altbgy."){
        @Override
        public String getFullName() {
            return "altábornagy";
        }
    }, VEZEREZREDES("vezds."){
        @Override
        public String getFullName() {
            return "vezérezredes";
        }
    };
    
    private String abbreviation;
    
    Rank(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    
    public String getAbbreviation() {
        return this.abbreviation;
    }
    
    public abstract String getFullName();
    
    @Override
    public String toString() {
        return getFullName();
    }
    
    /**
     * A rendfokozatnak megfelelő állománykategória meghatározására szolgál.
     */
    public String getRankCategory() {
        switch (this.toString()) {
            case "KOZKATONA": return "legénység";
            case "ORVEZETO": return "legénység";
            case "TIZEDES": return "legénység";
            case "SZAKASZVEZETO": return "legénység";
            case "ORMESTER": return "altiszt";
            case "TORZSORMESTER": return "altiszt";
            case "FOTORZSORMESTER": return "altiszt";
            case "ZASZLOS": return "altiszt";
            case "TORZSZASZLOS": return "altiszt";
            case "FOTORZSZASZLOS": return "altiszt";
            case "HADNAGY": return "tiszt";
            case "FOHADNAGY": return "tiszt";
            case "SZAZADOS": return "tiszt";
            case "ORNAGY": return "főtiszt";
            case "ALEZREDES": return "főtiszt";
            case "EZREDES": return "főtiszt";
            case "DANDARTABORNOK": return "főtiszt";
            case "VEZERORNAGY": return "főtiszt";
            case "ALTABORNAGY": return "főtiszt";
            case "VEZEREZREDES": return "főtiszt";
        }
        return null;
    }
    
}
