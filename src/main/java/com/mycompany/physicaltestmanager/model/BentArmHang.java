package com.mycompany.physicaltestmanager.model;

/**
 *A hajlított függés mozgásforma osztálya.
 */
public class BentArmHang extends ArmFormOfMovement {
    
    private int time;

    public BentArmHang(){}
    
    public BentArmHang(int time, int maleAgeGroupOne, int femaleAgeGroupOne, int maleAgeGroupTwo, int femaleAgeGroupTwo,
			int maleAgeGroupThree, int femaleAgeGroupThree, int maleAgeGroupFour, int femaleAgeGroupFour,
			int maleAgeGroupFive, int femaleAgeGroupFive) {
		super(maleAgeGroupOne, femaleAgeGroupOne, maleAgeGroupTwo, femaleAgeGroupTwo, maleAgeGroupThree, femaleAgeGroupThree,
				maleAgeGroupFour, femaleAgeGroupFour, maleAgeGroupFive, femaleAgeGroupFive);
		this.time = time;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}
}