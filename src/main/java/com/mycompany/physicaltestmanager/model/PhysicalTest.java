package com.mycompany.physicaltestmanager.model;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *A fizikai állapotfelmérés osztálya. Többek között tartalmazza a felmérés végrehajtás idejét, a végrehajtott mozgásformákat, azok eredményeit
 * valamint az összpontszámot.
 */
public class PhysicalTest {
    
    private Integer id;
    private int militaryId;
    @DateTimeFormat(pattern = "yyyy-MM-dd", fallbackPatterns = {"yyyy.MM.dd.", "yyyy.MM.dd",  "yyyy-MM-dd", "yyyy MM dd", "yyyyMMdd"})
    private LocalDate dateOfExecution;
    private int armFormOfMovement;
    private int armResult;
    private int trunkFormOfMovement;
    private int trunkResult;
    private int circulationFormOfMovement;
    private int circulationResult;
    private int score;
    private String bmi;
    private String bodyFatIndex;
    
    public PhysicalTest() {}
    
    public PhysicalTest(Integer id, int militaryId, LocalDate dateOfExecution, int armFormOfMovement, int armResult,
			int trunkFormOfMovement, int trunkResult, int circulationFormOfMovement, int circulationResult) {
		this.id = id;
		this.militaryId = militaryId;
		this.dateOfExecution = dateOfExecution;
		this.armFormOfMovement = armFormOfMovement;
		this.armResult = armResult;
		this.trunkFormOfMovement = trunkFormOfMovement;
		this.trunkResult = trunkResult;
		this.circulationFormOfMovement = circulationFormOfMovement;
		this.circulationResult = circulationResult;
	}
    
    public PhysicalTest(int militaryId, LocalDate dateOfExecution, int armFormOfMovement, int armResult,
			int trunkFormOfMovement, int trunkResult, int circulationFormOfMovement, int circulationResult) {
		this.militaryId = militaryId;
		this.dateOfExecution = dateOfExecution;
		this.armFormOfMovement = armFormOfMovement;
		this.armResult = armResult;
		this.trunkFormOfMovement = trunkFormOfMovement;
		this.trunkResult = trunkResult;
		this.circulationFormOfMovement = circulationFormOfMovement;
		this.circulationResult = circulationResult;
	}

    public PhysicalTest(Integer id, int militaryId, LocalDate dateOfExecution, int armFormOfMovement, int armResult,
			int trunkFormOfMovement, int trunkResult, int circulationFormOfMovement, int circulationResult, int score,
			String bmi, String bodyFatIndex) {
		this.id = id;
		this.militaryId = militaryId;
		this.dateOfExecution = dateOfExecution;
		this.armFormOfMovement = armFormOfMovement;
		this.armResult = armResult;
		this.trunkFormOfMovement = trunkFormOfMovement;
		this.trunkResult = trunkResult;
		this.circulationFormOfMovement = circulationFormOfMovement;
		this.circulationResult = circulationResult;
		this.score = score;
		this.bmi = bmi;
		this.bodyFatIndex = bodyFatIndex;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getMilitaryId() {
		return militaryId;
	}

	public void setMilitaryId(int militaryId) {
		this.militaryId = militaryId;
	}

	public LocalDate getDateOfExecution() {
		return dateOfExecution;
	}

	public void setDateOfExecution(LocalDate dateOfExecution) {
		this.dateOfExecution = dateOfExecution;
	}

	public int getArmFormOfMovement() {
		return armFormOfMovement;
	}

	public void setArmFormOfMovement(int armFormOfMovement) {
		this.armFormOfMovement = armFormOfMovement;
	}

	public int getArmResult() {
		return armResult;
	}

	public void setArmResult(int armResult) {
		this.armResult = armResult;
	}

	public int getTrunkFormOfMovement() {
		return trunkFormOfMovement;
	}

	public void setTrunkFormOfMovement(int trunkFormOfMovement) {
		this.trunkFormOfMovement = trunkFormOfMovement;
	}

	public int getTrunkResult() {
		return trunkResult;
	}

	public void setTrunkResult(int trunkResult) {
		this.trunkResult = trunkResult;
	}

	public int getCirculationFormOfMovement() {
		return circulationFormOfMovement;
	}

	public void setCirculationFormOfMovement(int circulationFormOfMovement) {
		this.circulationFormOfMovement = circulationFormOfMovement;
	}

	public int getCirculationResult() {
		return circulationResult;
	}

	public void setCirculationResult(int circulationResult) {
		this.circulationResult = circulationResult;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getBmi() {
		return bmi;
	}

	public void setBmi(String bmi) {
		this.bmi = bmi;
	}

	public String getBodyFatIndex() {
		return bodyFatIndex;
	}

	public void setBodyFatIndex(String bodyFatIndex) {
		this.bodyFatIndex = bodyFatIndex;
	}
}
