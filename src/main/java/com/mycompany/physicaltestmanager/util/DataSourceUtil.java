package com.mycompany.physicaltestmanager.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class DataSourceUtil {

	private DataSource ds;
    private static DataSourceUtil instance;

    private DataSourceUtil(String datasourceUrl, String datasourceDriver, String datasourceUsername, String datasourcePassword) {
    	DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName(datasourceDriver);
		ds.setUrl(datasourceUrl);
		ds.setUsername(datasourceUsername);
		ds.setPassword(datasourcePassword);
		this.ds = ds;
    }

    public static DataSourceUtil getInstance(String datasourceUrl, String datasourceDriver, String datasourceUsername, String datasourcePassword) {
    	instance = new DataSourceUtil(datasourceUrl, datasourceDriver, datasourceUsername, datasourcePassword);
        return instance;
    }

    public Connection getConnection() throws SQLException {
        Connection conn = ds.getConnection();
        return conn;
    }

    public void close(Connection conn) throws SQLException {
        if (conn != null && !conn.isClosed()) {
            conn.close();
        }
        conn = null;
    }
}
