package com.mycompany.physicaltestmanager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.mycompany.physicaltestmanager.util.DataSourceUtil;


@SpringBootApplication
public class PhysicalTestManagerApplication extends SpringBootServletInitializer {
	
    @Value("${spring.datasource.url}")
	private String datasourceUrl;
    @Value("${spring.datasource.driver-class-name}")
	private String datasourceDriver;
	@Value("${spring.datasource.username}")
	private String datasourceUsername;
	@Value("${spring.datasource.password}")
	private String datasourcePassword;
	
	public static void main(String[] args) {
		SpringApplication.run(PhysicalTestManagerApplication.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PhysicalTestManagerApplication.class);
	}
	
	@Bean
	public DataSourceUtil getDataSourceUtil() {
		return DataSourceUtil.getInstance(datasourceUrl, datasourceDriver, datasourceUsername, datasourcePassword);
	}
}
