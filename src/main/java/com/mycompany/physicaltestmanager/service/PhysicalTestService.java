package com.mycompany.physicaltestmanager.service;

import java.util.List;

import com.mycompany.physicaltestmanager.exception.PhysicalTestManagerDAOException;
import com.mycompany.physicaltestmanager.model.PhysicalTest;
import com.mycompany.physicaltestmanager.test.dao.PhysicalTestRepository;
import com.mycompany.physicaltestmanager.test.dao.impl.PhysicalTestRepositoryJDBCImpl;
import com.mycompany.physicaltestmanager.util.DataSourceUtil;

/**
 *
 * @author
 */
public class PhysicalTestService {
    
    private final PhysicalTestRepository testRepo;
    
    public PhysicalTestService(DataSourceUtil dsUtil) throws PhysicalTestManagerDAOException {
        this.testRepo = new PhysicalTestRepositoryJDBCImpl(dsUtil);
    }
    
    public List<PhysicalTest> findAll() throws PhysicalTestManagerDAOException {
        return testRepo.findAll();
    }

    public List<PhysicalTest> findByMilitaryId(int militaryId) throws PhysicalTestManagerDAOException {
        return testRepo.findByMilitaryId(militaryId);
    }

    public List<PhysicalTest> findByYear(int year) throws PhysicalTestManagerDAOException {
        return testRepo.findByYear(year);
    }
    
    public PhysicalTest findById(int id) throws PhysicalTestManagerDAOException {
		return testRepo.findById(id);
	}

    public void saveTest(PhysicalTest test) throws PhysicalTestManagerDAOException {
        testRepo.saveTest(test);
    }

    public void deleteTest(Integer id) throws PhysicalTestManagerDAOException {
        testRepo.deleteTest(id);
    }
}
