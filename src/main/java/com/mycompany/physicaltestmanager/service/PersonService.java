package com.mycompany.physicaltestmanager.service;

import java.util.List;

import com.mycompany.physicaltestmanager.exception.PhysicalTestManagerDAOException;
import com.mycompany.physicaltestmanager.model.Person;
import com.mycompany.physicaltestmanager.person.dao.PersonRepository;
import com.mycompany.physicaltestmanager.person.dao.impl.PersonRepositoryJDBCImpl;
import com.mycompany.physicaltestmanager.util.DataSourceUtil;

/**
 *
 * @author
 */
public class PersonService {
    
    private final PersonRepository personRepo;
    
    public PersonService(DataSourceUtil dsUtil) throws PhysicalTestManagerDAOException {
        this.personRepo = new PersonRepositoryJDBCImpl(dsUtil);
    }

    public List<Person> findAll() throws PhysicalTestManagerDAOException {
        return personRepo.findAll();
    }
    
    public List<Person> findByName(String name) throws PhysicalTestManagerDAOException {
        return personRepo.findByName(name);
    }
    
    public Person findByMilitaryId(int militaryId) throws PhysicalTestManagerDAOException {
        return personRepo.findByMilitaryId(militaryId);
    }
    
    public Person findById(int id) throws PhysicalTestManagerDAOException {
        return personRepo.findById(id);
    }

    public void savePerson(Person person) throws PhysicalTestManagerDAOException {
        personRepo.savePerson(person);
    }

    public void deletePerson(Integer id) throws PhysicalTestManagerDAOException {
        personRepo.deletePerson(id);
    }
}
