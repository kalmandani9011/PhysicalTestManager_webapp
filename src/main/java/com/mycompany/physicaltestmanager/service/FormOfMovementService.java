package com.mycompany.physicaltestmanager.service;

import com.mycompany.physicaltestmanager.exception.PhysicalTestManagerDAOException;
import com.mycompany.physicaltestmanager.formofmovement.dao.FormOfMovementRepository;
import com.mycompany.physicaltestmanager.formofmovement.dao.impl.FormOfMovementRepositoryJDBCImpl;
import com.mycompany.physicaltestmanager.model.*;
import com.mycompany.physicaltestmanager.util.DataSourceUtil;

/**
 *
 * @author
 */
public class FormOfMovementService {
    
    private static final int SCORETHRESHOLD = 220;
    private static final int MAXPUSHUP = 75;
    private static final int MINPUSHUP = 8;
    private static final int MAXPULLUP = 20;
    private static final int MINPULLUP = 1;
    private static final int MAXHANG = 70;
    private static final int MINHANG = 5;
    private static final int MAXSITUP = 90;
    private static final int MINSITUP = 6;
    private static final int MAXCRUNCH = 72;
    private static final int MINCRUNCH = 13;
    private static final int MAXKNEERAISE = 51;
    private static final int MINKNEERAISE = 10;
    private static final int MAXRUNNING3200 = 1330;
    private static final int MINRUNNING3200 = 2411;
    private static final int MAXRUNNING2000 = 1005;
    private static final int MINRUNNING2000 = 1808;
    private static final int MAXWALK1600 = 922;
    private static final int MINWALK1600 = 1826;
    private static final int MAXERGOMETRY = 320;
    private static final int MINERGOMETRY = 99;
    private static final int PUSHUPCOMBOBOXELEMENT = 0;
    private static final int PULLUPCOMBOBOXELEMENT = 1;
    private static final int HANGCOMBOBOXELEMENT = 2;
    private static final int SITUPCOMBOBOXELEMENT = 0;
    private static final int CRUNCHCOMBOBOXELEMENT = 1;
    private static final int KNEERAISECOMBOBOXELEMENT = 2;
    private static final int RUNNING3200COMBOBOXELEMENT = 0;
    private static final int RUNNING2000COMBOBOXELEMENT = 1;
    private static final int WALK1600COMBOBOXELEMENT = 2;
    private static final int ERGOMETRYCOMBOBOXELEMENT = 3;
    private final FormOfMovementRepository movementRepo;
    private String qualification = "MF";
    
    public FormOfMovementService(DataSourceUtil dsUtil) throws PhysicalTestManagerDAOException {
        this.movementRepo = new FormOfMovementRepositoryJDBCImpl(dsUtil);
    }
    
    public String getQualification() {
        return qualification;
    }
    
    /**
     * A fizikai felmérés pontszámának számítása.
     * @return Az összpontszámot adja vissza.
     */
    public int getScore(Person person, PhysicalTest test) throws PhysicalTestManagerDAOException {
        try {
            String ageGroup;
            qualification = "MF";
            int armScore = 0;
            if (test.getArmFormOfMovement() == HANGCOMBOBOXELEMENT) {
                ageGroup = determineHangAgeGroup(person.isMale(), person.getAge());
            } else {
                ageGroup = determineAgeGroup(person.isMale(), person.getAge());
            }
            armScore = armQuery(test.getArmFormOfMovement(), test.getArmResult(), ageGroup);
            ageGroup = determineAgeGroup(person.isMale(), person.getAge());
            int trunkScore = trunkQuery(test.getTrunkFormOfMovement(), test.getTrunkResult(), ageGroup);
            int circulationScore = circulationQuery(test.getCirculationFormOfMovement(), test.getCirculationResult(), ageGroup);
            int score = armScore + trunkScore + circulationScore;
            if (score < SCORETHRESHOLD || armScore == 0 || trunkScore == 0 || circulationScore == 0) {
                qualification = "NMF";
            }
            return score;
        } catch (PhysicalTestManagerDAOException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }
    
    /**
     * Az adott személy BMI indexét határozza meg. A BMI indexek külön korcsoportokra vannak meghatározva.
     * @param p A személy, akinek a BMI indexét kell meghatározni.
     * @return Megfelelt vagy nem megfelelt eredményt ad vissza.
     */
    public String determineBmi(Person p) throws PhysicalTestManagerDAOException {
        int[] weight = movementRepo.findBMIScore(p.getHeight(), determineBmiAgeGroup(p.isMale(), p.getAge()));
        int maxWeight = weight[0];
        int minWeight = weight[1];
        if (p.getWeight() >= minWeight && p.getWeight() <= maxWeight) {
            return "MF";
        } else {
            return "NMF";
        }
    }
    
    /**
     * Az adott személy testzsír indexét határozza meg. Függ a személy nemétől, a testzsír százalékától és a korától.
     * @param p A személy akinek a testzsír indexét kell meghatározni.
     * @return Megfelelő vagy nem megfelelő értéket ad vissza.
     */
    public String determineBodyFat(Person p) {
        if (p.isMale()) {
            if (p.getAge() <= 30) {
                if (p.getBodyFat() >= 8.0 && p.getBodyFat() <= 17.5) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (p.getAge() >= 31 && p.getAge() <= 40) {
                if (p.getBodyFat() >= 10.0 && p.getBodyFat() <= 20.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (p.getAge() >= 41 && p.getAge() <= 59) {
                if (p.getBodyFat() >= 11.0 && p.getBodyFat() <= 22.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (p.getAge() >= 60) {
                if (p.getBodyFat() >= 13.0 && p.getBodyFat() <= 25.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            }
        } else {
            if (p.getAge() <= 30) {
                if (p.getBodyFat() >= 20.0 && p.getBodyFat() <= 30.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (p.getAge() >= 31 && p.getAge() <= 40) {
                if (p.getBodyFat() >= 21.0 && p.getBodyFat() <= 33.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (p.getAge() >= 41 && p.getAge() <= 59) {
                if (p.getBodyFat() >= 23.0 && p.getBodyFat() <= 34.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            } else if (p.getAge() >= 60) {
                if (p.getBodyFat() >= 24.0 && p.getBodyFat() <= 36.0) {
                    return "MF";
                } else {
                    return "NMF";
                }
            }
        }
        return "";
    }

    /**
     * A legtöbb mozgásformára alkalmazandó korcsoport meghatározó metódus.
     * Az adatbázis megfelelő oszlopának kiválasztásához szükséges.
     * Amelyeket nem eszerint határozunk meg, azokat külön metódus szerint határozzuk meg. 
     * @param isMale A személy neme.
     * @param age A személy kora.
     * @return Az adatbázis oszlop nevét adja vissza.
     */
    private String determineAgeGroup(boolean isMale, int age) {
        if (isMale) {
            if (age < 25) {
                return "male_agegroup_one";
            } else if (age >= 25 && age <= 29) {
                return "male_agegroup_two";
            } else if (age >= 30 && age <= 34) {
                return "male_agegroup_three";
            } else if (age >= 35 && age <= 39) {
                return "male_agegroup_four";
            } else if (age >= 40 && age <= 44) {
                return "male_agegroup_five";
            } else if (age >= 45 && age <= 49) {
                return "male_agegroup_six";
            } else if (age >= 50 && age <= 54) {
                return "male_agegroup_seven";
            } else if (age >= 55 && age <= 59) {
                return "male_agegroup_eight";
            } else if (age >= 60 && age <= 65) {
                return "male_agegroup_nine";
            }
        } else {
            if (age < 25) {
                return "female_agegroup_one";
            } else if (age >= 25 && age <= 29) {
                return "female_agegroup_two";
            } else if (age >= 30 && age <= 34) {
                return "female_agegroup_three";
            } else if (age >= 35 && age <= 39) {
                return "female_agegroup_four";
            } else if (age >= 40 && age <= 44) {
                return "female_agegroup_five";
            } else if (age >= 45 && age <= 49) {
                return "female_agegroup_six";
            } else if (age >= 50 && age <= 54) {
                return "female_agegroup_seven";
            } else if (age >= 55 && age <= 59) {
                return "female_agegroup_eight";
            } else if (age >= 60 && age <= 65) {
                return "female_agegroup_nine";
            }
        }
        return "";
    }

    /**
     * A hajlított függés mozgásformára alkalmazandó korcsoport meghatározó metódus.
     * Az adatbázis megfelelő oszlopának kiválasztásához szükséges.
     * @param isMale A személy neme.
     * @param age A személy kora.
     * @return Az adatbázis oszlop nevét adja vissza.
     */
    private String determineHangAgeGroup(boolean isMale, int age) {
        if (isMale) {
            if (age < 31) {
                return "male_agegroup_one";
            } else if (age >= 31 && age <= 40) {
                return "male_agegroup_two";
            } else if (age >= 41 && age <= 50) {
                return "male_agegroup_three";
            } else if (age >= 51 && age <= 54) {
                return "male_agegroup_four";
            } else if (age >= 55 && age <= 65) {
                return "male_agegroup_five";
            }
        } else {
            if (age < 31) {
                return "female_agegroup_one";
            } else if (age >= 31 && age <= 40) {
                return "female_agegroup_two";
            } else if (age >= 41 && age <= 50) {
                return "female_agegroup_three";
            } else if (age >= 51 && age <= 54) {
                return "female_agegroup_four";
            } else if (age >= 55 && age <= 65) {
                return "female_agegroup_five";
            }
        }
        return "";
    }
    
    /**
     * A BMI index meghatározásához szükséges metódus.
     * Az adatbázis megfelelő oszlopának kiválasztásához szükséges.
     * @param isMale A személy neme.
     * @param age A személy kora.
     * @return Az adatbázis oszlop nevét adja vissza.
     */
    private String determineBmiAgeGroup(boolean isMale, int age) {
        if (isMale) {
            if (age < 26) {
                return "male_agegroup_one";
            } else if (age >= 26 && age <= 35) {
                return "male_agegroup_two";
            } else if (age >= 36 && age <= 45) {
                return "male_agegroup_three";
            } else if (age >= 46) {
                return "male_agegroup_four";
            }
        } else {
            if (age < 26) {
                return "female_agegroup_one";
            } else if (age >= 26 && age <= 35) {
                return "female_agegroup_two";
            } else if (age >= 36 && age <= 45) {
                return "female_agegroup_three";
            } else if (age >= 46) {
                return "female_agegroup_four";
            }
        }
        return "";
    }

    /**
     * A kar mozgásforma eredményének lekérdezéséhez szükséges metódus.
     * @param armType A grafikus felületen kiválasztott kar mozgásforma indexe.
     * @param armResult A kar mozgásforma elért eredménye.
     * @param ageGroup A korcsoport, amely az adatbázisból való kiválasztáshoz szükséges.
     * @return Az elért pontszámot adja vissza.
     */
    private int armQuery(int armType, int armResult, String ageGroup) throws PhysicalTestManagerDAOException {
        ArmFormOfMovement arm = null;
        switch (armType) {
            case PUSHUPCOMBOBOXELEMENT:
                arm = new PushUp();
                break;
            case PULLUPCOMBOBOXELEMENT:
                arm = new PullUp();
                break;
            case HANGCOMBOBOXELEMENT:
                arm = new BentArmHang();
                break;
        }
        if (arm instanceof PushUp) {
            if (armResult > MAXPUSHUP) {
                return 100;
            } else if(armResult < MINPUSHUP) {
                return 0;
            } else {
                return movementRepo.findScore("pushUp", armResult, ageGroup);
            }
        }
        if (arm instanceof PullUp) {
            if (armResult > MAXPULLUP) {
                return 100;
            } else if(armResult < MINPULLUP) {
                return 0;
            } else {
                return movementRepo.findScore("pullUp", armResult, ageGroup);
            }
        }
        if (arm instanceof BentArmHang) {
            if (armResult > MAXHANG) {
                return 100;
            } else if(armResult < MINHANG) {
                return 0;
            } else {
                return movementRepo.findScore("hang", armResult, ageGroup);
            }
        }
        return -1;
    }

    /**
     * A törzs mozgásforma eredményének lekérdezéséhez szükséges metódus.
     * @param trunkType A grafikus felületen kiválasztott törzs mozgásforma indexe.
     * @param trunkResult A törzs mozgásforma elért eredménye.
     * @param ageGroup A korcsoport, amely az adatbázisból való kiválasztáshoz szükséges.
     * @return Az elért pontszámot adja vissza.
     */
    private int trunkQuery(int trunkType, int trunkResult, String ageGroup) throws PhysicalTestManagerDAOException {
        TrunkFormOfMovement trunk = null;
        switch (trunkType) {
            case SITUPCOMBOBOXELEMENT:
                trunk = new SitUp();
                break;
            case CRUNCHCOMBOBOXELEMENT:
                trunk = new Crunch();
                break;
            case KNEERAISECOMBOBOXELEMENT:
                trunk = new KneeRaise();
                break;
        }
        if (trunk instanceof SitUp) {
            if (trunkResult > MAXSITUP) {
                return 100;
            } else if(trunkResult < MINSITUP) {
                return 0;
            } else {
                return movementRepo.findScore("sitUp", trunkResult, ageGroup);
            }
        }
        if (trunk instanceof Crunch) {
            if (trunkResult > MAXCRUNCH) {
                return 80;
            } else if(trunkResult < MINCRUNCH) {
                return 0;
            } else {
                return movementRepo.findScore("crunch", trunkResult, ageGroup);
            }
        }
        if (trunk instanceof KneeRaise) {
            if (trunkResult > MAXKNEERAISE) {
                return 80;
            } else if(trunkResult < MINKNEERAISE) {
                return 0;
            } else {
                return movementRepo.findScore("kneeRaise", trunkResult, ageGroup);
            }
        }
        return -1;
    }

    /**
     * A keringés mozgásforma eredményének lekérdezéséhez szükséges metódus.
     * @param circulationType A grafikus felületen kiválasztott keringés mozgásforma indexe.
     * @param circulationResult A keringés mozgásforma elért eredménye.
     * @param ageGroup A korcsoport, amely az adatbázisból való kiválasztáshoz szükséges.
     * @return Az elért pontszámot adja vissza.
     */
    private int circulationQuery(int circulationType, int circulationResult, String ageGroup) throws PhysicalTestManagerDAOException {
        CirculationFormOfMovement circulation = null;
        switch (circulationType) {
            case RUNNING3200COMBOBOXELEMENT:
                circulation = new Running3200();
                break;
            case RUNNING2000COMBOBOXELEMENT:
                circulation = new Running2000();
                break;
            case WALK1600COMBOBOXELEMENT:
                circulation = new Walk1600();
                break;
            case ERGOMETRYCOMBOBOXELEMENT:
                circulation = new Ergometry();
                break;
        }
        if (circulation instanceof Running3200) {
            if (circulationResult < MAXRUNNING3200) {
                return 160;
            } else if(circulationResult > MINRUNNING3200) {
                return 0;
            } else {
                return movementRepo.findScore("running3200", circulationResult, ageGroup);
            }
        }
        if (circulation instanceof Running2000) {
            if (circulationResult < MAXRUNNING2000) {
                return 160;
            } else if(circulationResult > MINRUNNING2000) {
                return 0;
            } else {
                return movementRepo.findScore("running2000", circulationResult, ageGroup);
            }
        }
        if (circulation instanceof Walk1600) {
            if (circulationResult < MAXWALK1600) {
                return 160;
            } else if(circulationResult > MINWALK1600) {
                return 0;
            } else {
                return movementRepo.findScore("walk1600", circulationResult, ageGroup);
            }
        }
        if (circulation instanceof Ergometry) {
            if (circulationResult > MAXERGOMETRY) {
                return 160;
            } else if(circulationResult < MINERGOMETRY) {
                return 0;
            } else {
                return movementRepo.findScore("ergometry", circulationResult, ageGroup);
            }
        }
        return -1;
    }
}
