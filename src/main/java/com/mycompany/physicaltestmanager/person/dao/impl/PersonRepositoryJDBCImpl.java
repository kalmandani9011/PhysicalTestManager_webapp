package com.mycompany.physicaltestmanager.person.dao.impl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mycompany.physicaltestmanager.exception.PhysicalTestManagerDAOException;
import com.mycompany.physicaltestmanager.model.Person;
import com.mycompany.physicaltestmanager.model.Rank;
import com.mycompany.physicaltestmanager.person.dao.PersonRepository;
import com.mycompany.physicaltestmanager.util.DataSourceUtil;

/**
 * A személyeket tartalmazó adatbázissal való kommunikációra alkalmas osztály.
 */
public class PersonRepositoryJDBCImpl implements PersonRepository {

    private DataSourceUtil dsUtil;
    private PreparedStatement findAll;
    private PreparedStatement insertPerson;
    private PreparedStatement updatePerson;
    private PreparedStatement deletePerson;
    private PreparedStatement findByMilitaryId;
    private PreparedStatement findById;
    private PreparedStatement findByName;
    
    public PersonRepositoryJDBCImpl(DataSourceUtil dsUtil) throws PhysicalTestManagerDAOException {
    	this.dsUtil = dsUtil;
    }
    
    public List<Person> findAll() throws PhysicalTestManagerDAOException {
        try {
        	findAll = dsUtil.getConnection().prepareStatement("SELECT * FROM persons");
            ResultSet all = findAll.executeQuery();
            List<Person> list = makeList(all);
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        } finally {
        	try {
				findAll.close();
			} catch (SQLException ex) {
				throw new PhysicalTestManagerDAOException(ex.getMessage());
			}
        }
    }
    
    public Person findByMilitaryId(int militaryId) throws PhysicalTestManagerDAOException {
        try {
        	findByMilitaryId = dsUtil.getConnection().prepareStatement("SELECT * FROM persons WHERE military_id = ?");
            findByMilitaryId.setInt(1, militaryId);
            ResultSet rsMilitaryId = findByMilitaryId.executeQuery();
            Person p = null;
            while (rsMilitaryId.next()) {
                p = makeOne(rsMilitaryId);
            }
            return p;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        } finally {
        	try {
        		findByMilitaryId.close();
			} catch (SQLException ex) {
				throw new PhysicalTestManagerDAOException(ex.getMessage());
			}
        }
    }
    
    public Person findById(int id) throws PhysicalTestManagerDAOException {
    	try {
    		findById = dsUtil.getConnection().prepareStatement("SELECT * FROM persons WHERE id = ?");
    		findById.setInt(1, id);
    		ResultSet rsId = findById.executeQuery();
    		Person p = null;
    		while (rsId.next()) {
                p = makeOne(rsId);
            }
            return p;
    	} catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        } finally {
        	try {
        		findById.close();
			} catch (SQLException ex) {
				throw new PhysicalTestManagerDAOException(ex.getMessage());
			}
        }
    }
    
    public List<Person> findByName(String name) throws PhysicalTestManagerDAOException {
        try {
        	findByName = dsUtil.getConnection().prepareStatement("SELECT * FROM persons WHERE name LIKE ?");
            findByName.setString(1, name);
            ResultSet rsName = findByName.executeQuery();
            List<Person> list = makeList(rsName);
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        } finally {
        	try {
        		findByName.close();
			} catch (SQLException ex) {
				throw new PhysicalTestManagerDAOException(ex.getMessage());
			}
        }
    }
    
    public void savePerson(Person p) throws PhysicalTestManagerDAOException {
        if (p.getId() == null) {
            insertPerson(p);
        } else {
            updatePerson(p);
        }
    }
    
    public void insertPerson(Person p) throws PhysicalTestManagerDAOException {
        try {
        	insertPerson = dsUtil.getConnection().prepareStatement("INSERT INTO persons (military_id, name, military_rank, unit,"
        			+ " birth_date, mothers_maiden_name, male, height, weight, body_fat) VALUES (?,?,?,?,?,?,?,?,?,?)");
            insertPerson.setInt(1, p.getMilitaryId());
            insertPerson.setString(2, p.getName());
            insertPerson.setString(3, p.getRank().name());
            insertPerson.setString(4, p.getUnit());
            insertPerson.setDate(5, Date.valueOf(p.getBirthDate()));
            insertPerson.setString(6, p.getMothersMaidenName());
            insertPerson.setBoolean(7, p.isMale());
            insertPerson.setInt(8, p.getHeight());
            insertPerson.setInt(9, p.getWeight());
            insertPerson.setDouble(10, p.getBodyFat());
            insertPerson.executeUpdate();
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        } finally {
        	try {
        		insertPerson.close();
			} catch (SQLException ex) {
				throw new PhysicalTestManagerDAOException(ex.getMessage());
			}
        }
    }

    public void updatePerson(Person p) throws PhysicalTestManagerDAOException {
        try {
        	updatePerson = dsUtil.getConnection().prepareStatement("UPDATE persons SET military_id = ?, name = ?, military_rank = ?, unit = ?,"
        			+ " birth_date = ?, mothers_maiden_name = ?, male = ?, height = ?, weight = ?, body_fat = ? WHERE id = ?");
            updatePerson.setInt(1, p.getMilitaryId());
            updatePerson.setString(2, p.getName());
            updatePerson.setString(3, p.getRank().name());
            updatePerson.setString(4, p.getUnit());	
            updatePerson.setDate(5, Date.valueOf(p.getBirthDate()));
            updatePerson.setString(6, p.getMothersMaidenName());
            updatePerson.setBoolean(7, p.isMale());
            updatePerson.setInt(8, p.getHeight());
            updatePerson.setInt(9, p.getWeight());
            updatePerson.setDouble(10, p.getBodyFat());
            updatePerson.setInt(11, p.getId());
            updatePerson.executeUpdate();
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        } finally {
        	try {
        		updatePerson.close();
			} catch (SQLException ex) {
				throw new PhysicalTestManagerDAOException(ex.getMessage());
			}
        }
    }
    
    public void deletePerson(int id) throws PhysicalTestManagerDAOException {
        try {
        	deletePerson = dsUtil.getConnection().prepareStatement("DELETE FROM persons WHERE id = ?");
            deletePerson.setInt(1, id);
            deletePerson.executeUpdate();
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        } finally {
        	try {
        		deletePerson.close();
			} catch (SQLException ex) {
				throw new PhysicalTestManagerDAOException(ex.getMessage());
			}
        }
    }
    
    /**
     * Segédmetódus, ami listát készít.
     */
    private List<Person> makeList(ResultSet rs) throws PhysicalTestManagerDAOException {
        try {
            List<Person> list = new ArrayList<>();
            while (rs.next()) {
                list.add(makeOne(rs));
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    /**
     * Segédmetódus, ami egy személy objektumot készít.
     */
    private Person makeOne(ResultSet rs) throws PhysicalTestManagerDAOException {
        try {
            Person p = new Person();
            p.setId(rs.getInt("id"));
            p.setMilitaryId(rs.getInt("military_id"));
            p.setName(rs.getString("name"));
            p.setRank(Rank.valueOf(rs.getString("military_rank")));
            p.setUnit(rs.getString("unit"));
            p.setBirthDate(rs.getDate("birth_date").toLocalDate());
            p.setMothersMaidenName(rs.getString("mothers_maiden_name"));
            p.setMale(rs.getBoolean("male"));
            p.setHeight(rs.getInt("height"));
            p.setWeight(rs.getInt("weight"));
            p.setBodyFat(rs.getDouble("body_fat"));
            return p;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

}
